# 2CzPN

This project provides the data associated with the paper entitled _Probing Disorder in 2CzPN using Core and Valence States_, as well as a notebook demonstrating the clustering approach used to identify representative molecules.

## Description

- Atomic structures for all the molecules are given in _relaxed\_structure_, _extracted\_structures_, _rigid\_structures_ and _rotated\_structures_
- The list of indices for the 42 representative molecules is given in _representative\_molecules.txt_
- The workflow for identifying representative molecules is presented in the jupyter notebook _identify\_representative\_molecules.ipynb_
- The folder _torsion\_angles_ contains the torsion angles for each set of molecules
- The folder _valence_ contains text files with the PBE and PBE0-calculated total energies, frontier orbital energies and band gaps for the different sets of molecules
- The folder _core_ contains text files with the PBE and PBE0-calculated core binding energies

## Dependencies

This project uses Scipy (https://scipy.org/) and PyBigDFT (https://gitlab.com/l_sim/bigdft-suite). Calculations were performed using BigDFT and MADNESS (https://github.com/m-a-d-n-e-s-s/madness/), as described in the associated paper.

## Authors and Acknowledgment

The authors are the same as those in the associated paper:
Nathalie K. Fernando, Martina Stella, William Dawson, Takahito Nakajima, Luigi Genovese, Anna Regoutz and Laura Ratcliff

For further acknowledgments, including funding and associated computer resources, please refer to the publication acknowledgments section.

## Link to paper:
_Probing Disorder in 2CzPN using Core and Valence States_, N. K. Fernando, M. Stella, W. Dawson, T. Nakajima, L. Genovese, A. Regoutz and L. E. Ratcliff, Phys. Chem. Chem. Phys., 24, 23329 (2022)

https://pubs.rsc.org/en/content/articlehtml/2022/cp/d2cp02638d

